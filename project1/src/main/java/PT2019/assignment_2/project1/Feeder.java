package PT2019.assignment_2.project1;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Feeder implements Runnable{
	
	ArrayList<CashierQueue> list = null;
	private AtomicInteger max = new AtomicInteger(0);
	private long max_time = 0;
	private long start_time = 0;
	private long end_time = 0;
	private int no_of_cust;
	private int queues_no;
	private int procesed_cust = 0;
	
	private Thread t = new Thread(this);
	private Thread t2 = new Thread(this);
	private Thread t3 = new Thread(this);
	
	public Feeder(int queues_no, int no_of_cust) throws InterruptedException {
		this.no_of_cust = no_of_cust;
		this.queues_no = queues_no;
		
		this.start_time = System.currentTimeMillis();	
		list = new ArrayList<CashierQueue>(queues_no);
		for(int i=0; i<queues_no; i++) {
			list.add(new CashierQueue());
		}
		//this.queueStart();
		//this.feederStart();
	}
	
	public void run() {
		try {
			int i=0;
			while(i<30) {
				Customer c = new Customer();
				Thread.sleep(2*c.getTime().get());
				if(c.getId().get() <= no_of_cust) process(c);
				getMaxC();
				//printQueues();
				//System.out.println();
				i++;
				if(i>10 && !checkAllEmpty()) break;	
			}
			Thread t = Thread.currentThread();
			t = null;
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void queueStart() {
		for(CashierQueue q : list) {
		    new Thread(q).start();
		}
	}
	
	public void process(Customer c) throws InterruptedException {
		CashierQueue q = null;
		q = getMinQueue();
		q.enque(c);
	}
	
	
	public CashierQueue getMinQueue() {
		CashierQueue min = list.get(0);
		int time = list.get(0).getTime().get();
		for(CashierQueue q : list) {
		    if(q.getTime().get()<time) {
		    	time = q.getTime().get();
		    	min = q;
		    }
		}
		return min;
	}
	
	public void getMaxC() {
		int current = 0;
		long last_time;
		for(CashierQueue q : list) {
			current += q.getQueueSize();
			last_time = System.currentTimeMillis();
			if(current > max.get()) {
				max.set(current);
				max_time = last_time;
			}
			end_time = last_time;
		}
	}
	
	public void printQueues() {
		for(CashierQueue q : list) {
			System.out.println(q);
		}
	}
	
	
	public String printQueue() {
		String temp="";
		long time = 0;
		if(max_time != 0) time = (max_time - start_time)/1000;
		temp+="Queues: "+ queues_no+ "\n\n"+ "Total customers: " + procesed_cust + "\nPeak hour: "+ max + " customers  at  " + time +" sec\n\n";
		procesed_cust=0;
		for(CashierQueue q : list) {
			procesed_cust+=q.getProcesedCust().get();
			temp+=q.toString();
			temp+="\n";
		}
		return temp;
	}
	
	public void feederStart() throws InterruptedException {
		this.queueStart();
		t.start();
		Thread.sleep(1200);
		t2.start();
		Thread.sleep(2400);
		t3.start();
	}
	
	public void feederSleep(int time) throws InterruptedException {
		Thread.sleep(time);
	}
	
	public void feederStop() {
		t = null;
		t2 = null;
		t3 = null;
	}
	
	public boolean checkAllEmpty() {
		int i=0;
		for(CashierQueue q : list) {
			if(!q.returnQueue().isEmpty()) i++;
		}
		if(i!=0) return true;
		else return false;
	}
}