package PT2019.assignment_2.project1;

import java.util.concurrent.atomic.AtomicInteger;

public class Customer{
	
	private int min_arriving_time = 200;
	private int max_arriving_time = 500;
	private int spawn_time;
	private int queue_id;
	private AtomicInteger cust_id;
	private static int count=0;
	
	public Customer() {
		cust_id = new AtomicInteger(count);
		count++;
		spawn_time = min_arriving_time + (int)((max_arriving_time - min_arriving_time +1)*Math.random());
	}
	
	public AtomicInteger getId() {
		return this.cust_id;
	}
	
	public AtomicInteger getTime() {
		return new AtomicInteger(this.spawn_time);
	}
	
	public void setQueue(int id) {
		this.queue_id = id;
	}	
}