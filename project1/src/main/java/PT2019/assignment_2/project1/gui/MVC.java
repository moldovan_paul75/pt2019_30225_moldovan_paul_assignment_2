package PT2019.assignment_2.project1.gui;

import javax.swing.*;

import PT2019.assignment_2.project1.Feeder;

public class MVC {
    public static void main(String[] args) throws InterruptedException {
    	
    	Model model = new Model();
    	View view = new View(model);
    	Controller controller = new Controller(model, view);
    }

}
