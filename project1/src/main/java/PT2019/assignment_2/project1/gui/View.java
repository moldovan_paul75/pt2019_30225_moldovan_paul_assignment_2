package PT2019.assignment_2.project1.gui;

import java.awt.*;
import javax.swing.*;

import PT2019.assignment_2.project1.Feeder;

import java.awt.event.*;
import java.util.concurrent.ExecutionException;

public class View extends JPanel implements Runnable{
	
	
	Feeder f = null;
	private Model model;
	private JTextArea textArea;
	private JPanel content1;
	private JPanel content2;
	private JPanel content3;
	private JPanel content4;
	private Thread thread = null;
	
	long start_time=0;
	long end_time=0;
	
	
    private JTextField input1 = new JTextField(5);
    private JTextField input2 = new JTextField(5);
    private JTextField input3 = new JTextField(5);
    private JTextField input4 = new JTextField(5);
    private JTextField input5 = new JTextField(5);
    private JTextField input6 = new JTextField(5);
    private JButton	startBtn = new JButton("Start");
    private JButton	stopBtn = new JButton("Stop");
    
    private JTextField input7 = new JTextField(5);
    private JTextField input8 = new JTextField(5);
    private JTextField input9 = new JTextField(5);
    private JTextField input10 = new JTextField(5);
	
	
	View(Model model) throws InterruptedException{
		
		this.model = model;
		//this.f = new Feeder(5, 100);
		
		JFrame window = new JFrame();
    	window.setTitle("Queues");
    	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	window.setSize(1080, 540);
    	window.setResizable(false);
    	window.setLocation(780, 220);
    	
    	content1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		textArea = new JTextArea(30, 60);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        content1.add(new JScrollPane(textArea));
        
        
        content2 = new JPanel();
        content2.setLayout(new BoxLayout(content2, BoxLayout.Y_AXIS));
        
        content3 = new JPanel();
        content3.setLayout(new BoxLayout(content3, BoxLayout.Y_AXIS));
        
        content4 = new JPanel();
        content4.setLayout(new BoxLayout(content4, BoxLayout.Y_AXIS));
        
        input1.setText("5"); 
        input2.setText("250");
        input3.setText("200");
        input4.setText("500");
        input5.setText("1000");
        input6.setText("2500");
        
        input7.setText("Queues no:");
        input7.setEditable(false);
        
        input8.setText("Customers no:");
        input8.setEditable(false);
        
        input9.setText("Ariving interval:");
        input9.setEditable(false);
        
        input10.setText("Serving interval:");
        input10.setEditable(false);
        
        content2.add(input7);
        content2.add(input1);
        
        content2.add(input8);
        content2.add(input2);
        
        content2.add(input9);
        content2.add(input3);
        content2.add(input4);
        
        content2.add(input10);
        content2.add(input5);
        content2.add(input6);
        //content2.setPreferredSize(new Dimension(50, 25));
        content2.setBorder(BorderFactory.createEmptyBorder(0,10,10,10)); 
        
        
        content3.add(startBtn);
        content3.add(stopBtn);
        
        content4.add(content2);
        content4.add(content3);
        
        window.add(content4, BorderLayout.WEST);
    	window.getContentPane().add(content1);
    	window.pack();
    	window.setVisible(true);
    	
    	
    	this.start();
	}
	
	public void start() throws InterruptedException {
		 if (thread == null) {
			  start_time = System.currentTimeMillis();
		      thread = new Thread(this);
		      f = new Feeder(5, 250);
		      thread.start();
		      f.feederStart();
		 }
	}

	public void stop() {
		thread = null;
		f.feederStop();
	}


	public void run() {
		      try {
		    	 while(true && thread!=null) {
		    		 end_time = (System.currentTimeMillis() - start_time)/1000;
		    		 Thread.sleep(100);
		    		 textArea.setText("Running for "+ end_time + " sec\n"+f.printQueue().toString());
		    		 if(end_time>30 && !f.checkAllEmpty()) break;
		    	 }
		      } 
		      catch (InterruptedException e) {
	    }
		thread = null;
	}
	
    void addStartListener(ActionListener start) {
        startBtn.addActionListener(start);
    }
    
    void addStopListener(ActionListener stop) {
        stopBtn.addActionListener(stop);
    }
	
}