package PT2019.assignment_2.project1.gui;

import java.awt.event.*;

public class Controller {
	
	private Model model;
	private View view;
	
	Controller(Model model, View view){
		this.model = model;
		this.view = view;
		
		view.addStartListener(new StartListener());
		view.addStopListener(new StoptListener());
	}
	
	class StartListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
        	try {
				view.start();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }	
	}
	

	class StoptListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
        	view.stop();
        }
	}
}
