package PT2019.assignment_2.project1;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

//Runnable subclass of queue
public class CashierQueue implements Runnable {
	
	private BlockingQueue<Customer> queue = new ArrayBlockingQueue<Customer>(128);
	private AtomicInteger id;
	private static int count=1;
	private AtomicInteger service_time; //time in milliseconds that
	private AtomicInteger customers_no; //number of processed customers
	private AtomicInteger current_customers_no;
	private int service_min_time = 1000; //default minimum service time
	private int service_max_time = 2500; //default maximum service time
	
	public CashierQueue() {
		this.id = new AtomicInteger(count);
		count++;
		current_customers_no = new AtomicInteger(0);
		service_time = new AtomicInteger(service_min_time + (int)((service_max_time - service_min_time +1)*Math.random()));
		customers_no = new AtomicInteger(0);
	}
	
	public void run() {
		try {
			while(true) {
				service_time = new AtomicInteger(service_min_time + (int)((service_max_time - service_min_time +1)*Math.random()));
				Thread.sleep(service_time.get());
				if(!queue.isEmpty()) {
					queue.take();
					customers_no.incrementAndGet();
					current_customers_no.decrementAndGet();
				}
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	public void setMinServiceTime(int service_min_time) {
		this.service_min_time = service_min_time;
	}
	
	public void setMaxServiceTime(int service_max_time) {
		this.service_max_time = service_max_time;
	}
	
	public AtomicInteger getServiceTime() {
		return this.service_time;
	}
	
	public AtomicInteger getQueueId() {
		return this.id;
	}
	
	public AtomicInteger getCustNo() {
		return this.customers_no;
	}
	
	
	public void enque(Customer c) throws InterruptedException {
		queue.put(c);
		current_customers_no.incrementAndGet();
	}

	
	public void deque() throws InterruptedException {
		if(!queue.isEmpty()) {
			Customer c = queue.take();
			customers_no.incrementAndGet();
			current_customers_no.decrementAndGet();
		}
	}
	
	public AtomicInteger getTime() {
		return new AtomicInteger((current_customers_no.get() *service_time.get())/1000);
	}
	
	public BlockingQueue<Customer> returnQueue() {
		return (BlockingQueue<Customer>)queue;
	}
	
	@Override
	public String toString() {
		String print = "";
		print+="Queue id: "+ getQueueId() + "\n" +"Current waiting time: " + getTime() + " sec\n"+ "Customers served: "+ customers_no + "\nCurrent customers: "+ current_customers_no + "\n   [";
		
		if(queue.isEmpty()) print+= " empty";
		else {
			for(Customer c : queue) {
				print+= "  " + c.getId();
			}
		}
		return print + "  ]\n";
		
	}
	public int getQueueSize() {
		return queue.size();
	}
	
	public AtomicInteger getProcesedCust() {
		return this.customers_no;
	}
	
}